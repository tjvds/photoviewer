
/*jslint browser : true , plusplus : true*/
/*global photoviewer*/

(function () {
    "use strict";
    var module = {
            images : [
                {src : "images/0004681.jpg", size : "103016", date : "Apr 29 2004", Creator : "pietje puk"},
                {src : "images/0004713.jpg", size : "168355", date : "Apr 29 2005", Creator : "pietje puk"},
                {src : "images/0004720.jpg", size : "113717", date : "Apr 29 2006", Creator : "pietje puk"},
                {src : "images/0004731.jpg", size : "162924", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004750.jpg", size : "149437", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004755.jpg", size : "112582", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004801.jpg", size : "102463", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004802.jpg", size : "95657", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004827.jpg", size : "134711", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004853.jpg", size : "116597", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004858.jpg", size : "108892", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004860.jpg", size : "94293", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004870.jpg", size : "150215", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004874.jpg", size : "148317", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004888.jpg", size : "97522", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004902.jpg", size : "132201", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004931.jpg", size : "125264", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004969.jpg", size : "84851", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0004971.jpg", size : "128014", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0006598.jpg", size : "139485", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0006630.jpg", size : "82114", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0006638.jpg", size : "93172", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0006680.jpg", size : "256007", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0006743.jpg", size : "191655", date : "Apr 29 2003", Creator : "pietje puk"},
                {src : "images/0006787.jpg", size : "169441", date : "Apr 29 2003", Creator : "pietje puk"}
            ],

            /**
             * Selected img
             */
            selectedImg : [{id : 0}],


            /**
             * viewstate : if : 0 window is not zoomed , if : 1 window is zoomed.
             */
            state : [{viewState: 0}],


            /**
             * Create page from array without detailed view.
             * @param img
             */
            createImageNodes: function (img) {
                var bodyNode, divNode, imgNode, i;

                bodyNode = document.getElementsByTagName("body")[0];

                divNode = document.createElement("div");

                divNode.setAttribute("Class", "Allimg");
                divNode.setAttribute("id", "imageContainer" + module.calGrid(img));

                bodyNode.appendChild(divNode);

                for (i = 0; i < img.length; i++) {
                    module.images[i].id = i;
                    imgNode = document.createElement("img");

                    imgNode.setAttribute("src", img[i].src);
                    imgNode.setAttribute("alt", img[i].Creator);
                    imgNode.setAttribute("id", "_" + i);
                    imgNode.setAttribute("oncontextmenu", "return false");

                    imgNode.addEventListener("contextmenu", module.removeNode, false);
                    imgNode.addEventListener("click", module.Zoom, false);

                    divNode.appendChild(imgNode);
                }
            },


            /**
             * Create detail view.
             * @param e clicked object
             * @constructor
             */
            Zoom : function (e) {
                var bodyNode, divNode, imgNode, pNode;

                if (e !== undefined) {
                    if (e.target.id.charAt(0) === "_") {
                        module.selectedImg[0].id = e.target.id.slice(1);
                    } else {
                        module.selectedImg[0].id = e.target.id;
                    }
                }

                if (document.getElementsByClassName("zoomedImage").length === 0) {
                    //Find body
                    bodyNode = document.getElementsByTagName("body")[0];

                    //Create detail view div
                    divNode = document.createElement("div");
                    divNode.setAttribute("Class", "zoomedImage");
                    bodyNode.appendChild(divNode);

                    //create img in div
                    imgNode = document.createElement("img");
                    imgNode.setAttribute("src", module.images[module.selectedImg[0].id].src);
                    imgNode.setAttribute("alt", module.images[module.selectedImg[0].id].Creator);
                    imgNode.setAttribute("class", "imgZoom");
                    imgNode.addEventListener("click", module.Zoom, false);
                    imgNode.setAttribute("oncontextmenu", "return false");

                    divNode.appendChild(imgNode);

                    // create info in div
                    pNode = document.createElement("p");
                    pNode.innerHTML = "Created by " + module.images[module.selectedImg[0].id].Creator + " on " + module.images[module.selectedImg[0].id].date + ". Size : " + module.images[module.selectedImg[0].id].size + "bytes.";

                    divNode.appendChild(pNode);
                } else {
                    document.body.innerHTML = "";
                    module.state[0].viewState = 0;
                    module.createImageNodes(module.images);
                }

            },


            /**
             * Remove image from array an refresh page.
             * @param e
             */
            removeNode : function (e) {
                var Exists;

                if (e.target.id.slice(1) > -1) {
                    module.images.splice(e.target.id.slice(1), 1);
                    Exists = document.getElementsByClassName("Allimg");

                    if (Exists.length === 0) {
                        module.createImageNodes(module.images);
                    } else {
                        document.body.innerHTML = "";
                        module.createImageNodes(module.images);
                    }
                }
            },


            /**
             * Calculate grid.
             * @param Array
             * @returns {number}
             */
            calGrid : function (Array) {
                return Math.round(Math.sqrt(Array.length));
            },

            //init ?? :)
            init: function () {
                photoviewer.createImageNodes(module.images);
            }

        };
    window.photoviewer = module;

}());

window.onload = function () {
    "use strict";
    photoviewer.init();
};


/**
 * Key event handeler.
 * @param e
 */
window.onkeyup = function (e) {
    "use strict";
    var key = e.keyCode || e.which;

    if (key === 39) {
        document.body.innerHTML = "";
        photoviewer.createImageNodes(photoviewer.images);
        photoviewer.state[0].viewState = 1;
        if (photoviewer.selectedImg[0].id < (photoviewer.images.length - 1)) {
            ++photoviewer.selectedImg[0].id;
        } else {
            photoviewer.selectedImg[0].id = 0;
        }
        photoviewer.Zoom();
    } else if (key === 37) {
        document.body.innerHTML = "";
        photoviewer.state[0].viewState = 1;
        photoviewer.createImageNodes(photoviewer.images);
        if (photoviewer.selectedImg[0].id === 0) {
            photoviewer.selectedImg[0].id = (photoviewer.images.length - 1);
        } else {
            --photoviewer.selectedImg[0].id;
        }
        photoviewer.Zoom();
    } else if (key === 32) {
        if (photoviewer.state[0].viewState === 0) {
            photoviewer.state[0].viewState = 1;
            photoviewer.Zoom();
        } else {
            photoviewer.state[0].viewState = 0;
            document.body.innerHTML = "";
            photoviewer.createImageNodes(photoviewer.images);
        }
    }
};









